package endpoints;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;
import payload.LoginCredentials;
import utils.URLs;

public class AuditorEndpoints {

	public static Response login(LoginCredentials loginCredentials) {
		return given().contentType("application/json").body(loginCredentials).when().post(URLs.login_URL);
	}

}
