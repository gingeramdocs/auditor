package testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import endpoints.AuditorEndpoints;
import io.qameta.allure.Story;
import io.restassured.response.Response;
import payload.LoginCredentials;
import utils.ReadAgentData;

public class AuditorTestcases {

	LoginCredentials loginCredentials = new LoginCredentials();

	@BeforeSuite
	public void initData() {
		ReadAgentData.getAgentDetails(loginCredentials);
	}

	@Story("Send OTP")
	@Test(priority = 1)
	public void sendOTP() {
		System.out.println("___________Send OTP___________");
		Response response = AuditorEndpoints.login(loginCredentials);
		response.then().log().all().extract().response();
		Assert.assertEquals(response.statusCode(), 200);

	}

}
